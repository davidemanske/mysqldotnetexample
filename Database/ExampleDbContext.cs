using Microsoft.EntityFrameworkCore;
using mysqldotnetexample.Models;

namespace mysqldotnetexample.Database
{
    public class ExampleDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql("server=localhost;database=exampledb;user=root;password=somesupersecretpw");
        }

        virtual public DbSet<ExampleModel> Models { get; set; }

    }
}