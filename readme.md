# Setup and Run It

## Docker MySql Database

Start by creating a new MySQL container running in docker. The following query will pull the latest version of mysql, tag it with the name 'examplemysql' and set an environment variable MYSQL_ROOT_PASSWORD to 'somesupersecretpw' and initial database MYSQL_DATABASE 'exampledb'. By default this image will be pulled and run, and will expose port (3306) for use, finally the -d will force the container to run in the background.

```
docker run --name examplemysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=somesupersecretpw -e MYSQL_DATABASE=exampledb -d mysql:latest
```

If you need to dive into the container to run any mysql commands, you can run the following:

```
docker exec -it examplemysql bash Run the Application
```

Update the connection string to point at your mysql database instance. The connection string can be found in the appsettings.json

```
dotnet run
--- OR ---
npm start
```

To see what other commands are available from Entity Framework, invoke the following:

```
dotnet ef -h EF
```

## Database Migrations

Entity framework elegantly handles migrations. After changing one of the data models that is mapped to a DbSet in the dbContext, you can add a migration by invoking the following command. Migrations are stored in the migrations folder and a corresponding database table will be added to your database to store the history of which migrations were applied.

```
dotnet ef migrations add 'name your change'
dotnet ef database update // to run the migration Docker Container Linking
```

## Docker Linking

Docker must be linked to other containers (ex. yourapplication) so they can access the mysql container by name and port. Simply add the --link argument to your docker run command to ensure that the name 'examplemysql' is available to that application.

```
docker run --name yourapplication --link examplemysql:mysql -d yourapplication
```

## Docker Persistent Storage

Writing persistent database content is outside the scope if this little example, but you would need to map a volume to the mysql container by adding the -v to the initial run argument.

```
-v /my/own/datadir:/var/lib/mysql
```
