﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace mysqldotnetexample.Migrations
{
    public partial class addedprofession : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Profession",
                table: "Models",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Profession",
                table: "Models");
        }
    }
}
