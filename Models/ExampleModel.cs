using System;
using System.ComponentModel.DataAnnotations;

namespace mysqldotnetexample.Models
{
    public class ExampleModel
    {
        public ExampleModel()
        {
            this.Id = Guid.NewGuid();
            this.CreatedOn = DateTime.UtcNow;
        }

        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Profession { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}