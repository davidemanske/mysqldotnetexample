﻿using System;
using System.Linq;
using mysqldotnetexample.Database;
using mysqldotnetexample.Models;

namespace mysqldotnetexample
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new ExampleDbContext())
            {

                Console.Write("Do you want to add a new example (y/N): ");

                var input = Console.ReadKey();
                if (input.Key == ConsoleKey.Y)
                {
                    string name = "";
                    while (string.IsNullOrEmpty(name))
                    {
                        Console.WriteLine("");

                        Console.Write("What do you want to name this next example object: ");
                        name = Console.ReadLine();
                    }

                    // example handled after migration
                    string profession = "";
                    while (string.IsNullOrEmpty(profession))
                    {
                        Console.Write("What is their profession: ");
                        profession = Console.ReadLine();
                    }

                    var example = new ExampleModel
                    {
                        Name = name,
                        Profession = profession
                    };
                    context.Add(example);
                    context.SaveChanges();
                }

                // Output the list
                Console.WriteLine("");
                Console.WriteLine("--------------");
                Console.WriteLine("Output Listing");
                Console.WriteLine("--------------");
                var examples = context.Models.OrderByDescending(o => o.CreatedOn).ToList();
                foreach (var example in examples)
                {
                    Console.WriteLine($"{example.Name} is a {example.Profession} and was created on {example.CreatedOn.ToShortDateString()} at {example.CreatedOn.ToShortTimeString()}");

                }
            }
        }
    }
}
